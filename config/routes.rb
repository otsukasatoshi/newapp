Rails.application.routes.draw do

  # デバイス
  devise_for :users, :controllers => {
    :omniauth_callbacks => "users/omniauth_callbacks"
  }

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # ルートページ
  root 'home#top'

  # ユーザー機能
  resources :users, only:[:show, :edit, :update] do
    member do
      get "like_rooms"
      get "review_rooms"
    end
  end

  # 投稿機能
  resources :rooms do
    resources :reviews, shallow: true # レビュー機能
    member do
      get "liking_users"
      get "review_users"
    end
    collection do
      get "search"
    end
  end

  # いいね機能
  post "/like/:room_id" => "likes#like", as: "like"
  delete "/unlike/:room_id" => "likes#unlike", as: "unlike"

  # お問い合わせ機能
  get "/contact" => "contacts#new"
  post "/contact" => "contacts#create"

end
