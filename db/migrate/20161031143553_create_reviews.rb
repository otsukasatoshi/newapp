class CreateReviews < ActiveRecord::Migration[5.0]
  def change
    create_table :reviews do |t|
      t.text :content
      t.integer :user_id
      t.integer :room_id

      t.timestamps null: false
    end
    add_index :reviews, [:user_id, :room_id]
  end
end
