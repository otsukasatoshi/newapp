class CreateRooms < ActiveRecord::Migration[5.0]
  def change
    create_table :rooms do |t|
      t.string :name
      t.string :picture
      t.string :access
      t.string :address
      t.string :rent
      t.string :floor_plan
      t.string :direction
      t.string :structure
      t.string :storey
      t.string :built
      t.string :exclusive_area
      t.text :notes
      t.integer :user_id

      t.timestamps null: false
    end
    add_index :rooms, [:user_id]
  end
end
