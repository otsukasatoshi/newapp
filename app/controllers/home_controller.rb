class HomeController < ApplicationController

  # トップページ
  def top
    @rooms = Room.all.order(created_at: :desc).page(params[:page])
  end

end
