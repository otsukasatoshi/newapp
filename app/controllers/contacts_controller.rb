class ContactsController < ApplicationController

  # お問い合わせ新規
  def new
    @contact = Contact.new
  end

  # お問い合わせ新規
  def creates
    @contact = Contact.new(contact_params)
    if @contact.valid?
      ContactMailer.new_contact(@contact).deliver
      flash[:success] = "お問い合わせを受け付けました"
      redirect_to root_path
    else
      render :new
    end
  end

  private

  def contact_params
    params.require(:contact).permit(:email, :messege)
  end

end
