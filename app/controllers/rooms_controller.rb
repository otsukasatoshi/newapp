class RoomsController < ApplicationController
  # ビフォーアクション
  before_action :set_room, only: [:show, :edit, :update, :destroy, :liking_users, :review_users]

  # 一覧
  def index
    @rooms = Room.all.order(created_at: :desc).page(params[:page])
  end

  # 詳細
  def show
    @reviews = @room.reviews
  end

  # 新規作成
  def new
    @room = Room.new
  end

  # 新規作成
  def create
    @room = current_user.rooms.build(room_params)
    if @room.save
      flash[:success] = "Post was successfully created."
      redirect_to @room
    else
      render :new
    end
  end

  # アップデート
  def edit
    #code
  end

  # アップデート
  def update
    if @room.update(room_params)
      flash[:success] = "Post was successfully updated."
      redirect_to @room
    else
      render :edit
    end
  end

  # 削除
  def destroy
    @room.destroy
    flash[:success] = "Post was successfully destroyed."
    redirect_to root_path
  end

  # いいねしてるユーザー
  def liking_users
    @users = @room.liking_users
  end

  # レビューしてるユーザー
  def review_users
    @users = @room.review_users
  end

  private

  # ビフォーアクション
  def set_room
    @room = Room.find(params[:id])
  end

  # ストロングパラメーター
  def room_params
    params.require(:room).permit(:name, :picture, :picture_cache, :access, :address, :rent, :floor_plan, :direction, :structure, :storey, :built, :exclusive_area, :notes, :latitude, :longitude, :user_id)
  end

end
