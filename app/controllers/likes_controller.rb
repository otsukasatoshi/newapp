class LikesController < ApplicationController
  # いいね
  def like
    @room = Room.find(params[:room_id])
    like = current_user.likes.build(room_id: @room.id)
    like.save
  end

  # いいね取り消し
  def unlike
    @room = Room.find(params[:room_id])
    like = current_user.likes.find_by(room_id: @room.id)
    like.destroy
  end
end
