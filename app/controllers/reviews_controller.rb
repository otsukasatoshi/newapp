class ReviewsController < ApplicationController

  # ビフォーアクション
  before_action :find_room, only:[:index, :new, :create]
  # ログイン必須
  before_action :authenticate_user!

  # レビュー一覧
  def index
    @reviews = @room.reviews
  end

  # レビュー詳細
  def show
    #code
  end

  # レビュー投稿
  def new
    @review = @room.reviews.build
  end

  # レビュー編集
  def edit
    @review = Review.find(params[:id])
  end

  # レビュー投稿
  def create
    @review = current_user.reviews.build(review_params)
    @review.room_id = @room.id
    if @review.save
      flash[:success] = "レビューを投稿しました。"
      redirect_to room_url(@review.room_id)
    else
      render :new
    end
  end

  # レビュー編集
  def update
    @review = Review.find(params[:id])
    if @review.update(review_params)
      flash[:success] = "レビューを更新しました。"
      redirect_to room_url(@review.room_id)
    else
      render :edit
    end
  end

  # レビュー削除
  def destroy
    @review = Review.find(params[:id])
    @review.destroy
    flash[:success] = "レビューを削除しました。"
    redirect_to room_url(@review.room_id)
  end

  private

  # ビフォーアクション
  def find_room
    @room = Room.find(params[:room_id])
  end

  # ストロングパラメーター
  def review_params
    params.require(:review).permit(:content)
  end

end
