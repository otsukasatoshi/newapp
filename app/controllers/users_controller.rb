class UsersController < ApplicationController
  # ログイン必須
  before_action :authenticate_user!
  # ビフォーアクション
  before_action :set_user, only: [:show, :edit, :update, :like_rooms, :review_rooms, :correct_user]
  # ログイン中のユーザーのビフォーアクション
  before_action :correct_user, only:[:edit, :update]

  # ユーザー詳細
  def show
    @rooms = @user.rooms.order(created_at: :desc)
    @review_rooms = @user.review_rooms.order(created_at: :desc)
    @like_rooms = @user.like_rooms.order(created_at: :desc)
  end

  # ユーザー編集
  def edit
    #code
  end

  # ユーザー編集
  def update
    if @user.update(user_params)
      flash[:success] = "User information updated."
      redirect_to @user
    else
      render :edit
    end
  end


  # ユーザーのいいね一覧
  def like_rooms
    @rooms = @user.like_rooms
  end

  # ユーザーのレビュー一覧
  def review_rooms
    @rooms = @user.review_rooms
  end

  private

  # ビフォーアクション
  def set_user
    @user = User.find(params[:id])
  end

  # ストロングパラメーター
  def user_params
    params.require(:user).permit(:name, :email, :image, :image_cache)
  end

  # ログインしているユーザーが正しいか
  def correct_user
    unless current_user?(@user)
      redirect_to root_path
    end
  end

end
