class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  # デバイスでユーザーネームを保存
  before_action :configure_permitted_parameters, if: :devise_controller?

  # ヘルパーの読み込み
  include ApplicationHelper

  private
  #  ユーザーネームで登録
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
  end

end
