module ApplicationHelper

  # ページタイトル変更
  def page_title
    title = "ここにサイト名"
    title = @page_title + " | " +title if @page_title
    title
  end

  # ログインしてれば編集できる
  def current_user?(user)
    current_user == user
  end

  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

end
