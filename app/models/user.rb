class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :omniauthable

  # アップローダー
  mount_uploader :image, ImageUploader

  # バリデーション
  validates :name, presence: true

  # アソシエーション
  has_many :rooms
  has_many :likes, dependent: :destroy
  has_many :like_rooms, through: :likes, source: :room
  has_many :reviews, dependent: :destroy
  has_many :review_rooms, through: :reviews, source: :room

  # twitterログイン
  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.name = auth.info.name
      user.image = auth.info.image
    end
  end

  # twitterからデータを取得
  def self.new_with_session(params, session)
    if session["devise.user_attributes"]
      new(session["devise.user_attributes"]) do |user|
        user.attributes = params
        user.valid?
      end
    else
      super
    end
  end

  # twitterでパスワードを要求しないようにする
  def password_required?
    super && provider.blank?
  end

  # passwordなしでも更新できるようにする
  def update_with_password(params, *options)
    if encrypted_password.blank?
      update_attributes(params, *options)
    else
      super
    end
  end

end
