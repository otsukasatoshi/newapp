require 'file_size_validator'
class Room < ApplicationRecord

  # アップローダー
  mount_uploader :picture, PictureUploader

  # バリデーション
  validates :picture, file_size: {maximum: 5.megabytes.to_i}
  validates :name, presence: true

  # google map
  geocoded_by :address
  after_validation :geocode, :if => :address_changed?

  # アソシエーション
  belongs_to :user
  has_many :likes, dependent: :destroy
  has_many :liking_users, through: :likes, source: :user
  has_many :reviews, dependent: :destroy
  has_many :review_users, through: :reviews, source: :user

end
